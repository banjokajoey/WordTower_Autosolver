WORDS_FILEPATH = './data/dict/dictionary_english.txt'


def binary_search(val_list, query, found_fn):
    # Init search indices
    lh = 0
    rh = len(val_list) - 1

    # Binary search for value
    while True:
        center = (lh + rh) / 2
        center_val = val_list[center]

        if found_fn(query, center_val):
            return True
        elif query < center_val:
            rh = center - 1
        else:
            lh = center + 1

        if lh > rh:
            break

    # Couldn't find that value
    return False


def str_equals(str1, str2):
    return str1 == str2


def is_prefix(query, string):
    return query == string[:len(query)]


class Dictionary(object):
    
    words = None

    def __init__(self, words_filepath=WORDS_FILEPATH):
        if Dictionary.words is None:
            words = []
            with open(words_filepath) as dict_file:
                for line in dict_file:
                    words.append(line.strip())
            Dictionary.words = words


    def is_word(self, query):
        return binary_search(Dictionary.words, query.upper(), str_equals)

        
    def is_prefix(self, query):
        return binary_search(Dictionary.words, query.upper(), is_prefix)
