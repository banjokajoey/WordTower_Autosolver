class SolutionsReference(object):

    solutions = None

    @staticmethod
    def get_puzzle_solution(puzzle_index):
        if SolutionsReference.solutions is None:
            SolutionsReference.load_solutions()

        if puzzle_index < 0 or puzzle_index > len(SolutionsReference.solutions):
            return None

        return SolutionsReference.solutions[puzzle_index]


    @staticmethod
    def load_solutions():
        if SolutionsReference.solutions:
            return

        solutions = []
        with open('./data/solutions/solutions.txt') as solutions_file:
            for line in solutions_file:
                line = line.strip()
                if line == '' or line[0] == '#':
                    continue
                solutions.append(line.split())

        SolutionsReference.solutions = solutions
