# NOTE
The easiest, most reproducible way to interact with these files is through bash scripts found in `REPO_ROOT/sh`.

# Program Flow
Screenshots -> **[WordTowerPuzzleExtractor]** -> Unsolved WordTowerPuzzle -> **[WordTowerPuzzleSolver]** -> Solved WordTowerPuzzle

# Classes

## Workhorses

### WordTowerPuzzleExtractor
* creates WordTowerPuzzles via public method `extract_puzzle(self, screenshot_filepath)`
* owns a convolutional neural net for letter classification, wrapped by the LetterClassifier class

### WordTowerPuzzleSolver
* runs through puzzle solution logic
* owns an english dictionary for word lookups
* can reference master list of solutions to check its work
* owns an english dictionary for word lookups

## Data storage

### WordTowerPuzzle 
* encapsulates all data related to a single puzzle
* manufactured by the WordTowerPuzzleExtractor

## Helper Classes

### LetterClassifer
* is a keras convolutional net
* can train a network from scratch
# can load the parameters from a previously trained network

## Modes of Interaction

### AutosolverGUI
* interface to run through Puzzle objects

