from letter_classifier import LetterClassifier
import numpy as np
import PIL.Image
import utils


# Puzzle extraction functions

def get_letter_images_from_screenshot(screenshot_img):
    puzzle_start_y = 0.125 * screenshot_img.height
    puzzle_end_y   = 0.625 * screenshot_img.height
    puzzle_img     = screenshot_img.crop((0, puzzle_start_y, screenshot_img.width, puzzle_end_y))
    puzzle_img     = puzzle_img.convert(mode='L')

    letter_bounds = find_letter_bounds(puzzle_img)
    letter_imgs = []
    for row in letter_bounds:
        letter_row = []
        for bounds in row:
            letter_row.append(puzzle_img.crop(bounds) if bounds else None)
        letter_imgs.append(letter_row)
    return letter_imgs


def get_solution_word_lengths_from_screenshot(screenshot_img):
    solution_start_y = 0.625 * screenshot_img.height
    solution_end_y   = 0.8 * screenshot_img.height
    solution_img     = screenshot_img.crop((0, solution_start_y, screenshot_img.width, solution_end_y))
    solution_img     = solution_img.convert(mode='L')
    
    solution_sizes = []
    img_array = np.asarray(solution_img)
    bg_color = img_array[0][0]
    row_idx = 0
    while row_idx < len(img_array):
        row = img_array[row_idx]
        solution_start = find_solution_in_row(row, bg_color)
        if solution_start != -1:
            count = 1
            in_box = True
            col = solution_start + 1
            while col < len(row):
                pixel_val = row[col]
                if pixel_val != bg_color and not in_box:
                    count += 1
                in_box = pixel_val != bg_color
                col += 1
            solution_sizes.append(count)

            while row_idx < len(img_array) and find_solution_in_row(img_array[row_idx], bg_color) != -1:
                row_idx += 1
        else:
            row_idx += 1

    return solution_sizes


def find_solution_in_row(row_array, bg_color):
    for i in range(len(row_array) / 2):
        if row_array[i] != bg_color:
            return i
    return -1


def find_letter_bounds(puzzle_img):
    # Image to array
    img_array = np.asarray(puzzle_img)

    # Find letter bounds
    x_bounds_arr = find_letter_x_bounds(img_array)
    y_bounds_arr = find_letter_y_bounds(img_array, x_bounds_arr)

    # Normalize bounds data
    letter_dim = min(find_min_range(x_bounds_arr), find_min_range(y_bounds_arr))
    conform_bounds_to_dim(x_bounds_arr, letter_dim)
    conform_bounds_to_dim(y_bounds_arr, letter_dim)        

    # Combine x and y bounds if there is a white pixel at their top left corner
    bounds = [ [None for i in range(len(x_bounds_arr))] for j in range(len(y_bounds_arr))]
    for col, x_bound in enumerate(x_bounds_arr):
        for row, y_bound in enumerate(y_bounds_arr):
            if img_array[y_bound[0], x_bound[0]] == 255:
                bounds[row][col] = (x_bound[0], y_bound[0], x_bound[1], y_bound[1])
    return bounds


def find_letter_x_bounds(img_array):
    # x bounds storage
    x_bounds_arr = []

    # Iterate through all image rows
    img_row = 0
    while img_row < len(img_array):
        row = img_array[img_row]
        if 255 in row:
            # We have found a row of letters
            img_col = 0
            in_letter = False
            letter_start = 0
            letter_end = 0

            # Find all letter bounds in this row
            while img_col < len(row):
                if row[img_col] == 255 and not in_letter:
                    in_letter = True
                    letter_start = img_col
                if row[img_col] != 255 and in_letter:
                    in_letter = False
                    x_bounds = (letter_start, img_col - 1)
                    if not list_contains_pair(x_bounds_arr, x_bounds):
                        x_bounds_arr.append(x_bounds)
                img_col += 1

            # Leave this row of letters
            while 255 in img_array[img_row]:
                img_row += 1
        else:
            img_row += 1

    # Sort and return
    x_bounds_arr.sort(key=lambda x_bound: x_bound[0])
    return x_bounds_arr


def find_letter_y_bounds(img_array, x_bounds_arr):
    # y bounds storage
    y_bounds_arr = []

    # Use x bounds to find y bounds
    for x_bounds in x_bounds_arr:
        row = 0
        col = x_bounds[0]
        while row < len(img_array):
            if img_array[row][col] == 255:
                # Found a letter
                letter_start = row

                # Find the end of the letter
                while img_array[row][col] == 255:
                    row += 1

                # Record y bounds
                y_bounds = (letter_start, row - 1)
                if not list_contains_pair(y_bounds_arr, y_bounds):
                    y_bounds_arr.append(y_bounds)
            else:
                row += 1

    # Sort and return
    y_bounds_arr.sort(key=lambda y_bound: y_bound[0])
    return y_bounds_arr


def find_min_range(bounds_arr):
    min_range = None
    for bounds in bounds_arr:
        bounds_range = bounds[1] - bounds[0]
        if min_range == None or bounds_range < min_range:
            min_range = bounds_range
    return min_range


def conform_bounds_to_dim(bounds_arr, dim):
    radius = dim / 2
    for i, bounds in enumerate(bounds_arr):
        c = (bounds[0] + bounds[1]) / 2
        bounds_arr[i] = (c - radius, c + radius)


def list_contains_pair(my_list, pair):
    tolerance = 5
    if pair in my_list:
        return True
    pair_a = pair[0]
    pair_b = pair[1]
    for a, b in my_list:
        if (pair_a <= a + tolerance and 
            pair_a >= a - tolerance and
            pair_b <= b + tolerance and
            pair_b >= b - tolerance):
            return True
    return False


def letter_images_to_letters(letter_imgs):
    if LetterClassifier.instance is None:
        LetterClassifier.instance = LetterClassifier()
        LetterClassifier.instance.load_word_tower_weights()

    puzzle_str = ''
    for row_idx, row in enumerate(letter_imgs):
        for col_idx, img in enumerate(row):
            letter = '.'
            if img:
                scaled_img = utils.scale_img_to_dim(img, 28, 28)
                letter = LetterClassifier.instance.predict_letter(np.asarray(img))
            puzzle_str += letter
    return puzzle_str
