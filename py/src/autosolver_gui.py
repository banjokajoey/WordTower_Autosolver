"""
File: autosolver_gui.py

This file launches a bunch of Tkinter panels inside 
of one master Tkinter window.

- AutosolverGUI           (root)
    - ScreenshotPanel     (extends AutosolverPanelABC)
    - ExtractedLettersPanel     (extends AutosolverPanelABC)
    - ParsedPuzzlePanel     (extends AutosolverPanelABC)
    - SolutionsPanel      (extends AutosolverPanelABC)

"""

import logging
import os
from puzzle import WordTowerPuzzle
import PIL.Image
import PIL.ImageTk
from Tkinter import *
import utils


logging.basicConfig(level=logging.INFO, format='%(message)s')


class AutosolverGUI(object):

    """
    The root Tkinter object. Loads all major UI panels and 
    provides a simple UI interface.
    """

    # Constants

    SCREENSHOT_DIR = './data/screenshots/'


    # Special methods

    def __init__(self, root):
        # Puzzle container (empty at start)
        self.puzzles = []

        # Window frame title
        root.title('Word Tower Autosolver')

        # Puzzle load and navigation
        nav_frame = Frame(root)
        prev_img = Button(nav_frame, text='<', command=self.prev_puzzle).pack(side=LEFT)
        self.load_training_puzzles_btn = Button(nav_frame, text='Load Puzzles', command=self.load_puzzles)
        self.load_training_puzzles_btn.pack(side=LEFT)
        self.puzzle_nav_label = Label(nav_frame, text='')
        self.puzzle_nav_label.pack(side=LEFT, padx=0)
        next_img = Button(nav_frame, text='>', command=self.next_puzzle).pack(side=LEFT)
        nav_frame.pack(pady=0)
        self.puzzles_loaded = False

        # Puzzle panels
        panel_frame = Frame(root)
        self.panels = []
        self.panels.append(ScreenshotPanel(panel_frame))
        self.panels.append(ExtractedLettersPanel(panel_frame))
        self.panels.append(ParsedPuzzlePanel(panel_frame))
        self.panels.append(SolutionsPanel(panel_frame))
        for i, panel in enumerate(self.panels):
            panel.pack(i)
        panel_frame.pack(padx=0, pady=10)


    # UI Callbacks

    def load_puzzles(self):
        for i, filename in enumerate(utils.get_png_filenames_in_directory(AutosolverGUI.SCREENSHOT_DIR)):
            self.puzzles.append(WordTowerPuzzle(i, os.path.join(AutosolverGUI.SCREENSHOT_DIR + filename)))

        self.puzzles_loaded = True
        self.load_training_puzzles_btn.destroy()
        self.puzzle_nav_label.pack(padx=20)
        
        self.current_puzzle = 0
        self.update()


    def next_puzzle(self):
        if self.puzzles_loaded:
            self.current_puzzle = min(self.current_puzzle + 1, len(self.puzzles))
            self.update()


    def prev_puzzle(self):
        if self.puzzles_loaded:
            self.current_puzzle = max(self.current_puzzle - 1, 0)
            self.update()


    # UI Update

    def update(self):
        # Lazy load puzzle data
        puzzle = self.puzzles[self.current_puzzle]
        if not puzzle.is_solved():
            puzzle.solve()
        
        # Update UI display
        for panel in self.panels:
            panel.update(puzzle)

        # Update UI puzzle nav
        self.puzzle_nav_label['text'] = '{0} / {1}'.format(self.current_puzzle + 1, len(self.puzzles))


class AutosolverPanelABC(object):

    """
    This is an abstract base class (ABC), meant to be the
    base UI object inserted into the autosolver gui.
    Do not instantiate this directly! The update function
    will throw an error.
    """

    def __init__(self, root, title):
        # New root
        self.root = Frame(root)

        # Title label
        title_frame = Frame(self.root)
        label = Label(title_frame, text=title, width=30)
        label.pack(pady=0)
        title_frame.grid(row=0, sticky='N')

        # Content frame
        self.content = Frame(self.root)


    def update(self, puzzle):
        raise NotImplementedError()


    def pack(self, col_idx):
        self.content.grid(row=1, sticky='S', ipady=100)
        self.root.grid(row=0, column=col_idx, sticky='N')


class ScreenshotPanel(AutosolverPanelABC):

    # Constants

    SCREENSHOT_SCALE = 0.3
    HOME_IMG_FILEPATH = './data/img/word_tower_home.png'


    # Special methods

    def __init__(self, root):
        # Super init
        AutosolverPanelABC.__init__(self, root, 'SCREENSHOT')

        # Set placeholder screenshot
        # Must keep a reference to the placeholder bc of bug in TkInter
        self.home_img = utils.get_GUI_image(ScreenshotPanel.HOME_IMG_FILEPATH, ScreenshotPanel.SCREENSHOT_SCALE)
        self.screenshot = Label(self.content, image=self.home_img)
        self.screenshot.pack()


    # Update UI

    def update(self, puzzle):
        logging.info('Update ScreenshotPanel')
        if not puzzle.screenshot_gui_image_is_cached():
            puzzle.cache_screenshot_gui_image(utils.get_GUI_image(puzzle.get_screenshot_filepath(), ScreenshotPanel.SCREENSHOT_SCALE))

        self.screenshot.configure(image = puzzle.get_cached_screenshot_gui_image())
        self.screenshot.image = puzzle.get_cached_screenshot_gui_image() # Hold a reference so Tkinter doesn't lose it. Known Tkinter bug.


class ExtractedLettersPanel(AutosolverPanelABC):

    # Special methods

    def __init__(self, root):
        # Super init
        AutosolverPanelABC.__init__(self, root, 'LETTERS')

        # Store placeholder letter image
        self.placeholder_letter_img = utils.get_GUI_image('./data/img/placeholder.png', 0.3)

        # Make letter grid
        self.cells = []
        puzzle_frame = Frame(self.content)
        for row in range(WordTowerPuzzle.MAX_DIM):
            for col in range(WordTowerPuzzle.MAX_DIM):
                cell = Label(puzzle_frame, image=self.placeholder_letter_img, bg='black')
                cell.grid(row=row, column=col)
                self.cells.append(cell)
        puzzle_frame.pack(padx=15, pady=15)


    # Update UI

    def update(self, puzzle):
        logging.info('Update ExtractedLettersPanel')

        # Load gui images if not loaded
        if puzzle.gui_letters is None:
            puzzle.gui_letters = []
            letter_imgs = puzzle.get_letter_images()
            for row_idx in range(WordTowerPuzzle.MAX_DIM):
                puzzle.gui_letters.append([])
                for col_idx in range(WordTowerPuzzle.MAX_DIM):
                    img = None
                    if row_idx < len(letter_imgs) and col_idx < len(letter_imgs[0]):
                        img = letter_imgs[row_idx][col_idx]
                    if img is not None:
                        img = PIL.ImageTk.PhotoImage(utils.scale_img_to_dim(img, 200, 200))
                    puzzle.gui_letters[-1].append(img)

        # Set letter images
        gui_letters = puzzle.gui_letters
        for row_idx in range(WordTowerPuzzle.MAX_DIM):
            for col_idx in range(WordTowerPuzzle.MAX_DIM):
                img = None
                if row_idx < len(gui_letters) and col_idx < len(gui_letters[0]) and gui_letters[row_idx][col_idx] is not None:
                    img = gui_letters[row_idx][col_idx]
                else:
                    img = self.placeholder_letter_img

                cell_idx = row_idx * WordTowerPuzzle.MAX_DIM + col_idx
                cell = self.cells[cell_idx]
                cell.configure(image = img)
                cell.image = img


class ParsedPuzzlePanel(AutosolverPanelABC):

    # Special methods

    def __init__(self, root):
        # Super init
        AutosolverPanelABC.__init__(self, root, 'PUZZLE')
        
        # Text grid
        puzzle_frame = Frame(self.content)
        self.cells = []
        for row in range(WordTowerPuzzle.MAX_DIM):
            for col in range(WordTowerPuzzle.MAX_DIM):
                cell = Label(puzzle_frame, text='.', font=('Courier', 50), padx=10, pady=5)
                cell.grid(row=row, column=col)
                self.cells.append(cell)
        puzzle_frame.pack(padx=15, pady=15)

        # Write word lengths
        self.word_lengths = Label(self.content, text='Word Lengths: {0}'.format(str([])))
        self.word_lengths.pack()


    # Update UI

    def update(self, puzzle):
        logging.info('Update ParsedPuzzlePanel')
        for row in range(WordTowerPuzzle.MAX_DIM):
            for col in range(WordTowerPuzzle.MAX_DIM):
                cell_idx = row * WordTowerPuzzle.MAX_DIM + col
                ltr = puzzle.get_cell_letter(row, col)
                self.cells[cell_idx]['text'] = '.' if ltr is None else ltr

        # Solution sizes
        self.word_lengths['text'] = 'Word Lengths: {0}'.format(str(puzzle.get_solution_word_lengths()))


class SolutionsPanel(AutosolverPanelABC):

    # Special methods

    def __init__(self, root):
        # Super init
        AutosolverPanelABC.__init__(self, root, 'SOLUTION')

        found_solutions_frame = Frame(self.content)
        self.found_solution_labels = []
        Label(found_solutions_frame, text='[FOUND]').pack()

        true_solutions_frame = Frame(self.content)
        self.true_solution_labels = []
        Label(true_solutions_frame, text='[ACTUAL]').pack()

        for i in range(WordTowerPuzzle.MAX_NUM_WORDS):
            found_label = Label(found_solutions_frame, text='---')
            found_label.pack()
            self.found_solution_labels.append(found_label)

            true_label = Label(true_solutions_frame, text='---')
            true_label.pack()
            self.true_solution_labels.append(true_label)

        found_solutions_frame.pack(pady=20)
        true_solutions_frame.pack(pady=20)

        self.is_correct = Label(self.content, text='(No Puzzle Loaded)')
        self.is_correct.pack()


    # Update UI

    def update(self, puzzle):
        logging.info('Update SolutionsPanel')
        for i in range(WordTowerPuzzle.MAX_NUM_WORDS):
            found_sol = puzzle.found_solutions[i] if i < len(puzzle.get_found_solutions()) else '---'
            self.found_solution_labels[i]['text'] = found_sol

            true_sol = puzzle.get_actual_solutions()[i] if i < len(puzzle.get_actual_solutions()) else '---'
            self.true_solution_labels[i]['text'] = true_sol

            self.is_correct['text'] = 'CORRECT' if puzzle.solution_is_correct() else 'ERROR'




# Script handle

if __name__ == '__main__':
    logging.info('Launching Word Tower Autosolver.')
    gui_root = Tk()
    autosolver_gui = AutosolverGUI(gui_root)
    gui_root.mainloop()
