import english_dictionary


# Public interface

def solve_puzzle(puzzle):
    solution = PuzzleGraph(puzzle).find_solution()
    return solution if solution else []


# Solver classes

class Cell(object):

    def __init__(self, ltr, row, col):
        self.ltr = ltr
        self.row = row
        self.col = col
        self.neighbors = []
        self.visited = False


    def add_neighbor(self, cell):
        if cell:
            self.neighbors.append(cell)

    def visit(self):
        self.visited = True


    def unvisit(self):
        self.visited = False


    def __str__(self):
        return 'ltr={0} row={1} col={2} w/ {3} neighbors'.format(self.ltr, self.row, self.col, len(self.neighbors))


class PuzzleGraph(object):

    # Constructor

    def __init__(self, puzzle):
        # Store puzzle
        self.puzzle = puzzle
        self.dictionary = english_dictionary.Dictionary()

        # Create cells
        self.cells = []
        for row_idx in range(puzzle.get_dimensions()):
            for col_idx in range(puzzle.get_dimensions()):
                ltr = puzzle.get_cell_letter(row_idx, col_idx)
                if ltr:
                    self.cells.append(Cell(ltr, row_idx, col_idx))
                else:
                    self.cells.append(None)

        # Link cells
        for cell in self.cells:
            self.set_neighbors(cell)


    def set_neighbors(self, cell):
        if not cell:
            return

        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                if i == 0 and j == 0:
                    continue

                neighbor = self.get_cell(cell.row + i, cell.col + j)
                if neighbor:
                    cell.add_neighbor(neighbor)

    # Get
    
    def get_cell(self, row_idx, col_idx):
        dim = self.puzzle.get_dimensions()
        if row_idx < 0 or row_idx >= dim or col_idx < 0 or col_idx >= dim:
            return None

        cell_idx = row_idx * dim + col_idx
        if cell_idx < 0 or cell_idx >= len(self.cells):
            return None
        return self.cells[cell_idx]


    # Solve

    def find_solution(self):
        return self.find_solution_recur([''], self.puzzle.get_solution_word_lengths())


    def find_solution_recur(self, curr_sol, word_lens, curr_cell=None):
        print (curr_sol)

        # Have we found it?
        if self.puzzle.is_solution(curr_sol):
            return curr_sol

        # Is this a dead end?
        curr_word = curr_sol[-1]
        if len(curr_word) > self.puzzle.longest_word_len or not self.dictionary.is_prefix(curr_word):
            return None

        # Is our current word a possible solution?
        if len(curr_word) in word_lens and self.dictionary.is_word(curr_word):
            curr_sol.append('')
            word_lens.remove(len(curr_word))
            sol = self.find_solution_recur(curr_sol, word_lens)
            if sol:
                return sol
            curr_sol.pop()
            word_lens.append(len(curr_word))

        # Extend our current word
        next_cells = curr_cell.neighbors if curr_cell else self.cells
        for cell in next_cells:
            if cell and not cell.visited:
                cell.visit()
                curr_sol[-1] = curr_word + cell.ltr
                sol = self.find_solution_recur(curr_sol, word_lens, cell)
                if sol:
                    return sol
                curr_sol[-1] = curr_word
                cell.unvisit()

        # Couldn't find anything
        return None


    def __str__(self):
        string = 'Word Tower Graph:\n'
        for cell in self.cells:
            if cell:
                string += '\t{0}\n'.format(str(cell))
        return string




