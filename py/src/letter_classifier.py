'''
Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras import backend as K
import numpy as np
import os
import PIL
import utils


# Hyperparameters

BATCH_SIZE = 128
NUM_CLASSES = 26
EPOCHS = 20
IMG_WIDTH, IMG_HEIGHT = 28, 28
CHANNELS_FIRST = K.image_data_format() == 'channels_first'
INPUT_SHAPE = (1, IMG_WIDTH, IMG_HEIGHT) if CHANNELS_FIRST else (IMG_WIDTH, IMG_HEIGHT, 1)


# Letter classifier class

class LetterClassifier(object):

	"""
	This class wraps a sizeable keras neural net. Therefore,
	it  follows the Singleton design pattern with the intention
	of preventing developers from mistakenly allocating more
	than one.

	This is necessary because my personal laptop sucks.
	"""

	# Filepaths

	DEFAULT_WEIGHTS_FILEPATH = './data/weights/cnn_weights.h5'

	# Singleton

	instance = None

	# Constructor

	def __init__(self, weights_filepath=None):
		model = Sequential()
		model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=INPUT_SHAPE))
		model.add(Conv2D(64, (3, 3), activation='relu'))
		model.add(MaxPooling2D(pool_size=(2, 2)))
		model.add(Dropout(0.25))
		model.add(Flatten())
		model.add(Dense(128, activation='relu'))
		model.add(Dropout(0.5))
		model.add(Dense(NUM_CLASSES, activation='softmax'))
		model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adadelta(), metrics=['accuracy'])
		self.model = model

		if weights_filepath:
			self.model.load_weights(weights_filepath)

		if LetterClassifier.instance is None:
			LetterClassifier.instance = self


	# Train

	def train(self, save_filepath=None):
		# Get training data
		(x_train, y_train), (x_test, y_test) = get_training_data()

		# Print status update
		print(x_train.shape[0], 'training samples')
		print(x_test.shape[0], 'testing samples')

		# Train CNN
		self.model.fit(x_train, y_train,
							batch_size=BATCH_SIZE,
							epochs=EPOCHS,
							verbose=1,
							validation_data=(x_test, y_test))

		# Evaluate CNN
		score = self.model.evaluate(x_test, y_test, verbose=0)

		# Print training results
		print('Test loss:', score[0])
		print('Test accuracy:', score[1])

		# Save network
		if save_filepath:
			self.model.save_weights(save_filepath)


	# Predict

	def predict_letter(self, image):
		letter_arr = rechannel_data(np.array([image]))
		probabilities = self.model.predict(letter_arr)[0]
		ltr_index = np.argmax(probabilities)
		return chr(ord('A') + ltr_index)


	# Load 

	def load_word_tower_weights(self):
		self.model.load_weights('./data/weights/cnn_weights.h5')



# Load training data

def get_letters_data(img_dir):
	filenames = utils.get_png_filenames_in_directory(img_dir)
	x_data = np.empty([len(filenames), 28, 28])
	y_data = np.empty(len(filenames), dtype=int)
	for i, img_name in enumerate(filenames):
		filepath = os.path.join(img_dir, img_name)
		x_data[i] = np.array(PIL.Image.open(filepath))
		y_data[i] = ord(img_name[0]) - ord('a')

	# Format input data (cross-platform? cross backend?)
	x_data = rechannel_data(x_data)	

	# Normalize x data
	x_data = x_data.astype('float32')
	x_data /= 255

	# Make y data categorical
	y_data = keras.utils.to_categorical(y_data, NUM_CLASSES)

	return (x_data, y_data)


def rechannel_data(data):
	if CHANNELS_FIRST:
		data = data.reshape(data.shape[0], 1, IMG_WIDTH, IMG_HEIGHT)
	else:
		data = data.reshape(data.shape[0], IMG_WIDTH, IMG_HEIGHT, 1)
	return data


def get_training_data():
	# The data, shuffled and split between train and test sets
	(x_train, y_train) = get_letters_data('../data/letters/train')
	(x_test, y_test) = get_letters_data('../data/letters/test')

	# Return processed data
	return (x_train, y_train), (x_test, y_test)

