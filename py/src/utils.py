import os
import PIL.Image
import shutil


def get_png_filenames_in_directory(directory_path):
    filenames = []
    for filename in os.listdir(directory_path):
        if filename.endswith('png') or filename.endswith('PNG'):
            filenames.append(filename)
    filenames.sort()
    return filenames


def scale_img_to_dim(img, target_width, target_height):
    width, height = img.size
    x_scale = float(target_width) / width
    y_scale = float(target_height) / height
    img.thumbnail((x_scale * width, y_scale * height), PIL.Image.ANTIALIAS)
    return img


def __letter_filename_to_index(filename):
    start_index = 1 if filename[0].isalpha() else 0
    return int(filename[start_index:filename.find('.')])


def natural_sort_letter_files(filenames):
    filenames.sort(key=__letter_filename_to_index)


def copy_file(src_path, old_name, dest_path, new_name):
    shutil.copy(os.path.join(src_path, old_name), os.path.join(dest_path, new_name))


def get_GUI_image(img_filepath, scale = 1.0):
    img = PIL.Image.open(img_filepath)
    width, height = img.size
    img.thumbnail((scale * width, scale * height), PIL.Image.ANTIALIAS)
    return PIL.ImageTk.PhotoImage(img)