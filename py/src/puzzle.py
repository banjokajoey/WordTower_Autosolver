import math
import PIL.Image
import puzzle_extractor
import puzzle_solver
import solution_lookup



class WordTowerPuzzle(object):
    """
    All data for an individual Word Tower puzzle. Comes with
    self-solving functionality.
    """

    MAX_DIM = 4
    MAX_NUM_WORDS = 3
    

    def __init__(self, puzzle_id, screenshot_filepath):
        # Metadata
        self.id = puzzle_id
        self.screenshot_filepath = screenshot_filepath
        self.loaded = False

        # Letter images
        self.letter_images = None
        self.gui_letters = None

        # Parsed data
        self.letters = None
        self.dimension = 0
        self.solution_word_lengths = []

        # GUI Data
        self.screenshot_gui_image_cache = None

        # Solution
        self.found_solutions = []
        self.actual_solutions = None


    def is_solved(self):
        return self.loaded


    def cache_screenshot_gui_image(self, image_to_cache):
        self.screenshot_gui_image_cache = image_to_cache


    def screenshot_gui_image_is_cached(self):
        return self.screenshot_gui_image_cache != None


    def get_cached_screenshot_gui_image(self):
        return self.screenshot_gui_image_cache


    def solve(self):
        # Early exit
        if self.is_solved():
            return

        # Load screenshot for puzzle extraction
        screenshot = PIL.Image.open(self.screenshot_filepath)
        self.letter_images = puzzle_extractor.get_letter_images_from_screenshot(screenshot)
        self.solution_word_lengths = puzzle_extractor.get_solution_word_lengths_from_screenshot(screenshot)

        # Load puzzle object
        self.letters = puzzle_extractor.letter_images_to_letters(self.letter_images)
        self.dimension = int(math.sqrt(len(self.letters)))

        # Get solutions
        self.actual_solutions = solution_lookup.SolutionsReference.get_puzzle_solution(self.id)
        self.longest_word_len = 0
        for word in self.actual_solutions:
            if len(word) > self.longest_word_len:
                self.longest_word_len = len(word)

        # Solve programmatically
        self.found_solutions = puzzle_solver.solve_puzzle(self)

        # Mark puzzle as fully loaded
        self.loaded = True


    def get_screenshot_filepath(self):
        return self.screenshot_filepath


    def get_letter_images(self):
        return self.letter_images


    def get_solution_word_lengths(self):
        return list(self.solution_word_lengths)


    def get_found_solutions(self):
        return self.found_solutions


    def get_actual_solutions(self):
        return self.actual_solutions
        

    def get_cell_letter(self, row, col):
        if row >= self.dimension or col >= self.dimension:
            return None
        ltr = self.letters[row * self.dimension + col]
        return ltr if ltr.isalpha() else None


    def get_dimensions(self):
        return self.dimension


    def solution_is_correct(self):
        if len(self.found_solutions) != len(self.actual_solutions):
            return False

        for found_sol in self.found_solutions:
            if found_sol not in self.actual_solutions:
                return False

        return True


    def is_solution(self, query):
        tmp = self.found_solutions
        self.found_solutions = query
        correct = self.solution_is_correct()
        self.found_solutions = tmp
        return correct
