import unittest
from src import dictionary


# JNet Feedforward Unit Tests

class TestDictionary(unittest.TestCase):

    dictionary = None

    # Unit test setup

    @classmethod
    def setUpClass(cls):
        TestDictionary.dictionary = dictionary.Dictionary('data/dict/dictionary_english.txt')


    # Unit tests (is_word)

    def test_is_word(self):
        self.assertTrue(TestDictionary.dictionary.is_word('A'))
        self.assertTrue(TestDictionary.dictionary.is_word('ZEBRA'))
        self.assertTrue(TestDictionary.dictionary.is_word('MOTHER'))
        self.assertTrue(TestDictionary.dictionary.is_word('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))
        self.assertTrue(TestDictionary.dictionary.is_word('SLOW'))


    def test_is_not_word(self):
        self.assertFalse(TestDictionary.dictionary.is_word(''))
        self.assertFalse(TestDictionary.dictionary.is_word('AQRS'))
        self.assertFalse(TestDictionary.dictionary.is_word('ZEBRASSS'))
        self.assertFalse(TestDictionary.dictionary.is_word('Z'))


    # Unit tests (is_prefix)

    def test_is_prefix(self):
        self.assertTrue(TestDictionary.dictionary.is_prefix(''))
        self.assertTrue(TestDictionary.dictionary.is_prefix('A'))
        self.assertTrue(TestDictionary.dictionary.is_prefix('ZEBR'))
        self.assertTrue(TestDictionary.dictionary.is_prefix('MOTHER'))
        self.assertTrue(TestDictionary.dictionary.is_prefix('ABCDEFGHIJKLMNOPQRSTUVWXY'))
        self.assertTrue(TestDictionary.dictionary.is_prefix('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))


    def test_is_not_prefix(self):
        self.assertFalse(TestDictionary.dictionary.is_prefix('ABCDEFGHIJKLMNOPQRSTUVWXYZZ'))
        self.assertFalse(TestDictionary.dictionary.is_prefix('FJK'))
        self.assertFalse(TestDictionary.dictionary.is_prefix('ZLL'))



# Main method

if __name__ == '__main__':
    unittest.main()