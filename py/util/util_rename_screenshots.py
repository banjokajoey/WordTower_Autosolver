import os
import sys


def make_img_filename(stage_num, level_num):
	return "stage_{0}_level_{1}.png".format(str(stage_num).zfill(2), str(level_num).zfill(2))


def rename_word_tower_screenshots(directory_path):
	# Get image file names sorted lexicographically. This will be
	# the assumed correct stage-level ordering.
	filenames = []
	for filename in os.listdir(directory_path):
		if filename.endswith('PNG') or filename.endswith('png'):
			filenames.append(filename)
	filenames.sort()

	# CAUTION
	raw_input ("About to rename {0} images. Continue? ".format(len(filenames)))
	
	# Rename all image files
	for img_index in range(len(filenames)):
		stage_num = (img_index / 40) + 1
		level_num = (img_index % 40) + 1
		old_name = "{0}/{1}".format(directory_path, filenames[img_index])
		new_name = "{0}/{1}".format(directory_path, make_img_filename(stage_num, level_num))
		os.rename(old_name, new_name)


if __name__ == '__main__':
	rename_word_tower_screenshots('../img/')