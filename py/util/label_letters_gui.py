from __future__ import print_function
import math
import PIL.Image
import PIL.ImageTk
from Tkinter import *
import utils


# Constants

BATCH_SIZE = 100
LTRS_PER_ROW = 20
LETTER_IMG_DIR = '../data/letters/normal/'


# GUI for hand labeling letter images

class LabelLettersGUI(object):

    # Constructor

    def __init__(self, root):
        # Load letter images
        self.letter_imgs = []
        filenames = utils.get_png_filenames_in_directory(LETTER_IMG_DIR)
        utils.natural_sort_letter_files(filenames)
        for filename in filenames:
            filepath = '{0}{1}'.format(LETTER_IMG_DIR, filename)
            ltr_img = PIL.ImageTk.PhotoImage(PIL.Image.open(filepath))
            self.letter_imgs.append(ltr_img)

        # Initialize GUI
        root.title('Letter Labeler')
        self.label_units = []

        # Initialize letter rows
        current_row = None
        for i in range(BATCH_SIZE):
            if i % LTRS_PER_ROW == 0:
                current_row = Frame(root)
            self.label_units.append(LabelLetterUnit(current_row, i, self.text_entered, self.letter_imgs[0]))
            if i % LTRS_PER_ROW - 1 == 0:
                current_row.pack()

        # Initialize batch nav controls
        batch_nav_frame = Frame(root)
        prev_batch = Button(batch_nav_frame, text='<', command=self.prev_batch)
        prev_batch.pack(side=LEFT)
        self.batch_label = Label(batch_nav_frame, text='')
        self.batch_label.pack(side=LEFT)
        next_batch = Button(batch_nav_frame, text='>', command=self.next_batch)
        next_batch.pack(side=LEFT)
        batch_nav_frame.pack()

        # Initialize batch save controls
        self.batch_index = 0
        self.confirm_btn = Button(root, text='Confirm Batch', width=20, command=self.save_batch)
        self.confirm_btn.pack()

        # Initialize focus
        self.label_units[0].focus_set()

        # Refresh gui
        self.update()


    # Update

    def update(self):
        index_start = self.batch_index * BATCH_SIZE
        for i, label_unit in enumerate(self.label_units):
            ltr_index = index_start + i
            if ltr_index < len(self.letter_imgs):
                label_unit.update(self.letter_imgs[ltr_index])
            else:
                label_unit.clear()
        self.batch_label['text'] = 'Batch {0} / {1}'.format(self.batch_index + 1, int(self.num_batches()))


    # Callbacks

    def text_entered(self, index_var, string_var):
        index = index_var.get()
        string_var.set(string_var.get().upper())
        if len(string_var.get()) == 1 and index < BATCH_SIZE - 1:
            self.label_units[index + 1].focus_set()


    def save_batch(self):
        labels_filepath = '../data/letters/labels/batch_{0}.csv'.format(str(self.batch_index).zfill(2))
        with open(labels_filepath, 'w') as label_file:
            for i, label_unit in enumerate(self.label_units):
                img_index = self.batch_index * BATCH_SIZE + i
                hand_label = label_unit.get_label_val()
                print('{0},{1}'.format(img_index, hand_label), file=label_file)


    def prev_batch(self):
        if self.batch_index > 0:
            self.batch_index -= 1
            self.update()


    def next_batch(self):
        if self.batch_index < self.num_batches() - 1:
            self.batch_index += 1
            self.update()


    # Helpers

    def num_batches(self):
        num_ltrs = len(self.letter_imgs)
        return math.ceil(num_ltrs / float(BATCH_SIZE))


# Labeling unit

class LabelLetterUnit(object):

    def __init__(self, root, index, callback, default_img):
        unit_frame = Frame(root)

        self.img_gui = Label(unit_frame, image=default_img)
        self.img_gui.pack()

        index_var = IntVar()
        index_var.set(index)
        self.sv = StringVar()
        self.sv.trace('w', lambda name, index, mode, sv=self.sv: callback(index_var, self.sv))
        self.text_enter = Entry(unit_frame, width=3, textvariable=self.sv)
        self.text_enter.pack()

        unit_frame.pack(side=LEFT)


    def update(self, ltr_img):
        self.img_gui.configure(image = ltr_img)
        self.img_gui.image = ltr_img
        self.sv.set('')


    def clear(self):
        self.sv.set('---')


    def focus_set(self):
        self.text_enter.focus_set()


    def get_label_val(self):
        return self.sv.get()


if __name__ == '__main__':
    gui_root = Tk()
    labeller_gui = LabelLettersGUI(gui_root)
    gui_root.mainloop()