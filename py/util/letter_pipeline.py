import os
import PIL
import puzzle_extractor
import random
import string
import utils


SCREENSHOT_DIR        = '../data/screenshots/'
LABELS_CSV_DIR        = '../data/letters/labels/'
RAW_LETTER_DIR        = '../data/letters/0_raw/'
NORMAL_LETTER_DIR     = '../data/letters/1_normal/'
SORTED_LETTERS_DIR    = '../data/letters/2_sorted/'
REDUCED_LETTERS_DIR   = '../data/letters/3_reduced/'
FILLED_LETTERS_DIR    = '../data/letters/4_fill'
AUGMENTED_LETTERS_DIR = '../data/letters/5_augment'
TRAINING_DIR = '../data/letters/train'
TEST_DIR = '../data/letters/test'


def extract_all_letters():
    filenames = utils.get_png_filenames_in_directory(SCREENSHOT_DIR)
    letter_total = 0
    for i, filename in enumerate(filenames):
        filepath = SCREENSHOT_DIR + filename
        letter_imgs = puzzle_extractor.get_letter_images(PIL.Image.open(filepath))
        letters_found = 0
        for ltr_row in letter_imgs:
            for img in ltr_row:
                if img:
                    img.save('{0}{1}.png'.format(RAW_LETTER_DIR, letter_total))
                    letters_found += 1
                    letter_total += 1
        print ('{0}: scraped {1} letters'.format(filename, letters_found))
    print ("found {0} letters".format(letter_total))


def normalize_letters():
    filenames = utils.get_png_filenames_in_directory(RAW_LETTER_DIR)
    print ('normalizing {0} letters'.format(len(filenames)))
    for filename in filenames:
        img = PIL.Image.open('{0}{1}'.format(RAW_LETTER_DIR, filename))
        img = utils.scale_img_to_dim(img, 28, 28)
        img.save('{0}{1}'.format(NORMAL_LETTER_DIR, filename))


def sort_letters():
    letters_map = {}
    for filename in os.listdir(LABELS_CSV_DIR):
        if filename.endswith('csv'):
            with open('{0}{1}'.format(LABELS_CSV_DIR, filename)) as csv_file:
                for line in csv_file:
                    index, letter = line.strip().split(',')
                    if letter not in letters_map:
                        letters_map[letter] = []
                    letters_map[letter].append(index)

    for letter, indices in letters_map.items():
        letter = letter.lower()
        letter_dir = '{0}{1}/'.format(SORTED_LETTERS_DIR, letter)
        os.makedirs(letter_dir)
        for i, letter_idx in enumerate(indices):
            ltr_filename = '{0}.png'.format(letter_idx)
            desired_filename = '{0}{1}.png'.format(letter, i)
            utils.copy_file(NORMAL_LETTER_DIR, letters_dir, desired_filename)


def reduce_letters():
    for ltr in string.ascii_lowercase:
        src_path = os.path.join(SORTED_LETTERS_DIR, ltr)
        dest_path = os.path.join(REDUCED_LETTERS_DIR, ltr)
        os.makedirs(dest_path)
        filenames = utils.get_png_filenames_in_directory(src_path)
        utils.natural_sort_letter_files(filenames)
        for i in range(min(10, len(filenames))):
            utils.copy_file(src_path, filenames[i], dest_path, filenames[i])


def fill_letters():
    for ltr in string.ascii_lowercase:
        src_path = os.path.join(REDUCED_LETTERS_DIR, ltr)
        dest_path = os.path.join(FILLED_LETTERS_DIR, ltr)
        os.makedirs(dest_path)
        src_files = utils.get_png_filenames_in_directory(src_path)
        for i in range(10):
            src_filename = src_files[i % len(src_files)]
            dest_filename = '{0}{1}.png'.format(ltr, i)
            utils.copy_file(src_path, src_filename, dest_path, dest_filename)


def augment_letters():
    dims = [30, 32, 34, 36]
    for ltr in string.ascii_lowercase:
        src_path = os.path.join(FILLED_LETTERS_DIR, ltr)
        dest_path = os.path.join(AUGMENTED_LETTERS_DIR, ltr)
        os.makedirs(dest_path)
        src_files = utils.get_png_filenames_in_directory(src_path)
        for i, src_file in enumerate(src_files):
            src_img = PIL.Image.open(os.path.join(src_path, src_file))
            utils.copy_file(src_path, src_file, dest_path, src_file)
            for j, dim in enumerate(dims):
                new_index = len(src_files) + i * len(dims) + j
                new_img = src_img.copy()
                new_img = new_img.resize((dim, dim))
                left = (dim / 2) - 14
                right = (dim / 2) + 14
                new_img = new_img.crop((left, left, right, right))
                new_filename = '{0}{1}.png'.format(ltr, new_index)
                new_img.save(os.path.join(dest_path, new_filename))


def split_train_and_test():
    for ltr in string.ascii_lowercase:
        src_path = os.path.join(AUGMENTED_LETTERS_DIR, ltr)
        src_files = utils.get_png_filenames_in_directory(src_path)
        utils.natural_sort_letter_files(src_files)
        test_file_indices = [0, random.randint(1, 9)]
        for i, src_file in enumerate(src_files):
            dest_path = TRAINING_DIR
            if i in test_file_indices:
                dest_path = TEST_DIR
            utils.copy_file(src_path, src_file, dest_path, src_file)


if __name__ == '__main__':
    import time
    start_time = time.time()
    split_train_and_test()
    print("--- {0} seconds ---".format(round(time.time() - start_time, 2)))